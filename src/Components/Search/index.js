import React, { useState } from "react";
import logo from "../../assets/logo-nav.png";
import "./style.css";

const Header = props => {
  const [Search, setSearch] = useState([]);
  return (
    <>
      <div className="Header">
        <div className="logo">
          <img src={logo} className="Company" alt="Empresas" />
        </div>

        <input
          type="text"
          id="search"
          name="search"
          className={Search ? "search" : "searchOpen"}
          placeholder="Pesquisar"
        />
      </div>

      <div>
        <ul className="companyList">
          {Search.map(company => (
            <li>
              <header />
              <strong>{company.investor_name}</strong>
              <span>{company.title}</span>
            </li>
          ))}
        </ul>
      </div>

      <div className="container">
        <h3>Clique na Busca para Iniciar</h3>
      </div>
    </>
  );
};

export default Header;
