import React from "react";
import "./style.css";

export default function Company() {
  return (
    <>
      <div className="companyBody">
        <p className="compnayTitle">Empresa 1</p>
        <p className="compnaySubtitle">Negócio</p>
        <p className="compnayCountry">Brasil</p>
      </div>
    </>
  );
}
