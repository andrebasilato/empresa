import React, { useState } from "react";
import api from "../../services/api";
import logo from "../../assets/logo.png";
import "./style.css";

export default function Login({ history }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();
    if (!email || !password) {
      setError("Preencha e-mail e senha para continuar!");
    } else {
      try {
        const response = await api.post("/sign_in", { email, password });
        setError("");
        localStorage.setItem("access-token", response.headers["access-token"]);
        localStorage.setItem("client", response.headers["client"]);
        localStorage.setItem("uid", response.headers["uid"]);
        history.push("/dashboard");
      } catch (err) {
        setError(
          "Houve um problema com o login, verifique seu e-mail e sua senha."
        );
      }
    }
  }

  return (
    <>
      <img src={logo} alt="Empresas" />
      <h2>BEM-VINDO AO EMPRESAS</h2>
      <p>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </p>

      {error && <p className="error">{error}</p>}

      <form onSubmit={handleSubmit}>
        <input
          type="email"
          id="email"
          className="email"
          placeholder="E-mail"
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <input
          type="password"
          id="password"
          className="pass"
          placeholder="Senha"
          value={password}
          onChange={event => setPassword(event.target.value)}
        />

        <button className="btn" type="submit">
          Entrar
        </button>
      </form>
    </>
  );
}
